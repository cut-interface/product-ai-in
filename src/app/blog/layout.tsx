"use client";
import React, { useState, useEffect, ReactNode } from 'react';
import SingleContent from "./SingleContent";
import SingleRelatedPosts from "./SingleRelatedPosts";
import Navleft from "@/components/navleft/navleft";

const Layout = ({ children }: { children: ReactNode }) => {
  const [shouldHideNavleft, setShouldHideNavleft] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      // Set the condition to hide/show based on the screen width
      if (window.innerWidth <= 900) {
        setShouldHideNavleft(true);
      } else {
        setShouldHideNavleft(false);
      }
    };

    // Call handleResize when the window size changes
    window.addEventListener('resize', handleResize);

    // Call handleResize immediately when the component is mounted
    handleResize();

    // Cleanup event listener when the component is unmounted
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  return (
    <div>
      {children}

      {/* SINGLE MAIN CONTENT */}
      <div className="container mt-10">
        {!shouldHideNavleft && (
          <div className="fixed h-screen left-0 top-10 md:top-20">
            <Navleft />
          </div>
        )}

        <SingleContent />
      </div>

      {/* RELATED POSTS */}
      <div className="container mt-10">
        <SingleRelatedPosts />
      </div>
    </div>
  );
};

export default Layout;