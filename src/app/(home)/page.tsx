"use client";
import React, { useState, useEffect } from 'react';
import Product from "@/components/A_Product-sl/product";
import Blog from "@/components/Blog/Blog";
import RightNav1 from "@/components/A_Product-sl/RightNav1";
import Navleft from "@/components/navleft/navleft";
import { DEMO_AUTHORS } from "@/data/authors";
import { DEMO_POSTS_NEWS } from "@/data/posts";

const MAGAZINE1_POSTS = DEMO_POSTS_NEWS.filter((_, i) => i >= 8 && i < 16);

const Homepage: React.FC = () => {
  const [shouldHideNavleft, setShouldHideNavleft] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      // Thiết lập điều kiện ẩn hiện dựa trên kích thước màn hình
      if (window.innerWidth <= 900) {
        setShouldHideNavleft(true);
      } else {
        setShouldHideNavleft(false);
      }
    };

    // Gọi handleResize khi kích thước màn hình thay đổi
    window.addEventListener('resize', handleResize);

    // Gọi handleResize ngay khi component được mount
    handleResize();

    // Cleanup event listener khi component unmount
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);
  
  return (
    <div className="container mx-auto">
    <div className="flex flex-col md:flex-row space-x-5 ">
      {!shouldHideNavleft && (
        <div className="fixed h-screen left-0">
          <Navleft />
        </div>
      )}
  
      <div className="w-full md:w-3/4 lg:w-4/5 xl:w-5/6">
        <div className="overflow-hidden">
          <div className="mx-4 md:mx-10 mb-6 md:mb-0">
            <Product
              heading="Suggested"
              subHeading=""
              authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
            />
          </div>
  
          <div className="mx-4 md:mx-10 mb-6 md:mb-0">
            <Product
              heading="Text to image with Adobe Express"
              subHeading=""
              authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
            />
          </div>
  
          <div className="mx-4 md:mx-10 mb-6 md:mb-0">
            <Product
              heading="Recent"
              subHeading=""
              authors={DEMO_AUTHORS.filter((_, i) => i < 10)}
            />
          </div>
  
          <div className="mx-4 md:mx-10 mt-10 mb-28">
            <Blog posts={MAGAZINE1_POSTS} />
          </div>
        </div>
      </div>
  
      <div className="md:w-1/4 lg:w-1/5 xl:w-1/6 md:ml-2 mt-3 md:mt-0">
        <div className="md:fixed md:top-28 lg:right-32">
          <RightNav1 />
        </div>
      </div>
    </div>
  </div>
  );
};

export default Homepage;