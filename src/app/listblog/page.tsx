"use client";
import React, { useState, useEffect } from 'react';
import Navleft from "@/components/navleft/navleft";
import SectionLargeSlider from "@/app/(home)/SectionLargeSlider";
import BackgroundSection from "@/components/BackgroundSection/BackgroundSection";
import Heading1 from "@/components/A_ListBlog/Heading1";
import {
  DEMO_POSTS,
  DEMO_POSTS_AUDIO,
  DEMO_POSTS_GALLERY,
  DEMO_POSTS_VIDEO,
} from "@/data/posts";
import SectionBlog from "@/components/A_ListBlog/SectionBlog";
import SectionBlog1 from "@/components/A_ListBlog/SectionBlog1";
import SectionBlog2 from "@/components/A_ListBlog/SectionBlog2";
import SectionMagazine9 from "@/components/A_ListBlog/SectionMagazine9";
import SectionMagazine2 from "@/components/A_ListBlog/SectionMagazine2";

//

const MAGAZINE1_POSTS = DEMO_POSTS.filter((_, i) => i >= 8 && i < 16);
const MAGAZINE2_POSTS = DEMO_POSTS.filter((_, i) => i >= 0 && i < 7);
//

const PageHome = ({}) => {
  const [shouldHideNavleft, setShouldHideNavleft] = useState(false);

useEffect(() => {
  const handleResize = () => {
    // Thiết lập điều kiện ẩn hiện dựa trên kích thước màn hình
    if (window.innerWidth <= 900) {
      setShouldHideNavleft(true);
    } else {
      setShouldHideNavleft(false);
    }
  };

  // Gọi handleResize khi kích thước màn hình thay đổi
  window.addEventListener('resize', handleResize);

  // Gọi handleResize ngay khi component được mount
  handleResize();

  // Cleanup event listener khi component unmount
  return () => {
    window.removeEventListener('resize', handleResize);
  };
}, []);



  return (
    <div className="nc-PageHome container mb-20">
            {!shouldHideNavleft && (
        <div className="fixed h-screen left-0">
          <Navleft />
        </div>
      )}

      <div className="lg:ml-14">

        <div className="relative py-16">

          <SectionMagazine2
            heading="Newest authors"
            subHeading="Say hello to future creator potentials"
            posts={MAGAZINE2_POSTS}
          />
        </div>

        <SectionMagazine9 />

            <div className="relative">
              <div className="">
                <SectionBlog posts={MAGAZINE1_POSTS} />
              </div>
            </div>


            <div className="relative">
              <div className="">
                <SectionBlog1 posts={MAGAZINE1_POSTS} />
              </div>
            </div>

            <div className="relative">
              <div className="">
                <SectionBlog2 posts={MAGAZINE1_POSTS} />
              </div>
            </div>
   
      </div>
    </div>
  );
};

export default PageHome;
