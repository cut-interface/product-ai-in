import React, { FC } from "react";

export interface SingleTitleProps {
  desc: string;
  className?: string;
  mainClass?: string;
}

const SingleTitle: FC<SingleTitleProps> = ({
  mainClass = "text-neutral-900 text-sm md:text-sm md:!leading-[120%] lg:text-sm dark:text-neutral-100",
  className = "",
  desc,
}) => {
  return (
    <p className={className + " " + mainClass + " max-w-4xl "} title={desc}>
      {desc}
    </p>
  );
};

export default SingleTitle;
