"use client";
import React, { FC, useState, useEffect } from "react";
import CardAuthor from "@/components/CardAuthor/CardAuthor";
import WidgetHeading1 from "@/components/WidgetHeading1/WidgetHeading1";
import { DEMO_AUTHORS } from "@/data/authors";
import { PostAuthorType } from "@/data/types";
import Link from "next/link";

const authorsDemo: PostAuthorType[] = DEMO_AUTHORS.filter((_, i) => i < 20);

export interface WidgetAuthorsProps {
  className?: string;
  authors?: PostAuthorType[];
}

const WidgetAuthors: FC<WidgetAuthorsProps> = ({
  className = "bg-gray-100 dark:bg-neutral-800",
  authors = authorsDemo,
}) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Điều chỉnh ngưỡng cần thiết
    };

    handleResize(); // Kiểm tra ban đầu
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div
      className={`nc-WidgetAuthors rounded-sm overflow-hidden ${
        isMobile ? "container mx-auto" : "" // Thêm class "mx-auto" cho di chuyển tới trung tâm trên màn hình rộng
      } flex flex-col flex-1 ${className}`}
      style={{ maxHeight: "100%", overflowY: "auto" }}
    >
      <WidgetHeading1 className="h-20" title="Apps" />
      <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
        {authors.map((author) => (
          <CardAuthor
            className="p-2 xl:p-3 hover:bg-neutral-300 dark:hover:bg-neutral-700"
            key={author.id}
            author={author}
          />
        ))}
      </div>

      <WidgetHeading1 title="FEATURED" />
      <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
        {authors.map((author) => (
          <CardAuthor
            className="p-2 xl:p-3 hover:bg-neutral-300 dark:hover:bg-neutral-700"
            key={author.id}
            author={author}
          />
        ))}
      </div>

      <WidgetHeading1 title="Category" />
      <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
        <div className="hover:bg-neutral-200 flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3">
          <Link href="/" className={`nc-CardAuthor flex items-center ${isMobile ? "mt-4" : ""} ${className}`}>
            <span>
              <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
                {/* Mã SVG */}
              </svg>
            </span>
            <div>
              <span className={`mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}>
                Update
              </span>
            </div>
          </Link>
        </div>
      </div>

      {/* Các mục Category khác ở đây */}

    </div>
  );
};

export default WidgetAuthors;