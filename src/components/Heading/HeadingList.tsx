"use client"
import React, {FC, HTMLAttributes, ReactNode, useState } from "react";
import Nav from "@/components/Nav/Nav";
import NavItem from "@/components/Heading/NavItem";

export interface HeadingProps extends HTMLAttributes<HTMLHeadingElement> {
  fontClass?: string;
  desc?: ReactNode;
  tabs?: string[] ;
}

const Heading1: React.FC<HeadingProps> = ({
  children,
  desc = "Discover the most outstanding articles in all topics of life. ",
  className = "mb-4 md:mb-4 text-neutral-900 dark:text-neutral-50",
  tabs = ["Desktop", "Phone", "Web"],
  ...args
}) => {
    const [tabActive, setTabActive] = useState<string>(tabs[0]);

    const handleClickTab = (item: string) => {
      if (item === tabActive) {
        return;
      }
      setTabActive(item);
    };

  return (
    <div
      className={`nc-Section-Heading relative flex sm:items-end ${className}`}
    >

        <h2
          className={`text-sm md:text-sm lg:text-sm font-semibold mt-4 ml-5`}
          {...args}
        >
          {children || `Section Heading`}
        </h2>
      </div>
    // </div>
  );
};

export default Heading1;
