"use client";

import React, { FC } from "react";
import HeadingPro from "@/components/Heading/HeadingPro";
import { PostAuthorType } from "@/data/types";
import Cart20 from "@/components/Card20/Card20";
import MySlider from "@/components/MySlider";

export interface SectionSliderNewAuthorsProps {
  className?: string;
  heading: string;
  subHeading: string;
  authors: PostAuthorType[];
  itemPerRow?: number;
}

const Product: FC<SectionSliderNewAuthorsProps> = ({
  heading = "Suggestions for discovery",
  subHeading = "Popular places to recommend for you",
  className = "",
  authors,
  itemPerRow = 4,
}) => {
  return (
    <div className={`nc-SectionSliderNewAuthors ${className}`}>
      <HeadingPro desc={subHeading}>
        {heading}
      </HeadingPro>
      <MySlider
        itemPerRow={itemPerRow}
        data={authors}
        renderItem={(item, index) => (
          <Cart20 key={index} author={item} />
        )}
      />
    </div>
  );
};

export default Product;