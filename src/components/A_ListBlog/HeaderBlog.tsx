"use client";

import React, { FC, useState } from "react";
import Heading from "@/components/Heading/HeadingBlog";
import Nav from "@/components/Nav/Nav";
import NavItem from "@/components/Blog/NavItem";
import Button from "../Blog/Button";
import { ArrowRightIcon } from "@heroicons/react/24/outline";

export interface HeaderFilterProps {
  tabs?: string[];
  heading: string;
}

const HeaderFilter: FC<HeaderFilterProps> = ({
  tabs = ["All items", "Garden", "Fitness", "Design"],
  heading = "🎈 Latest Articles",
}) => {
  const [tabActive, setTabActive] = useState<string>(tabs[0]);

  const handleClickTab = (item: string) => {
    if (item === tabActive) {
      return;
    }
    setTabActive(item);
  };

  return (
    <div className="flex flex-col mt-8 relative">
      <Heading>{heading}</Heading>
    </div>
  );
};

export default HeaderFilter;
