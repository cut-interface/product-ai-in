"use client"
import React, { FC, useState, useEffect } from "react";
import { PostAuthorType } from "@/data/types";
import Avatar from "@/components/Avatar/Avatar";
import Link from "next/link";

export interface CardAuthorBoxProps {
  className?: string;
  author: PostAuthorType;
}

const Cart20: FC<CardAuthorBoxProps> = ({ className = "", author }) => {
  const { displayName, href = "/", avatar, jobName, desc, count } = author;


  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);


  return (
    <div
      
      className={`w-full h-64 flex flex-col justify-center p-3 sm:p-4 border-2 border-solid hover:border-gray-300 rounded-xl bg-white dark:bg-neutral-900 ${className}`}
    >
      <Avatar
        sizeClass="w-12 h-12 text-2xl -mt-3"
        radius="rounded-xl"
        imgUrl={avatar}
        userName={displayName}
      />
      <div className="mt-3">
        <span className="block mt-1 text-xs text-neutral-500 dark:text-neutral-400">
          @{jobName}
        </span>
        <h2 className="text-xs sm:text-sm font-medium line-clamp-1">
          {displayName}
        </h2>
        <span className=" mt-3 text-xs text-neutral-500 dark:text-neutral-400 line-clamp-2">
          {desc}
        </span>
      </div>
      <Link href={'/product'}>
      {isMobile && (
      <div className="py-2 w-full sm:w-28 px-4 mt-4 bg-neutral-100 dark:bg-neutral-800 border-2 border-solid border-black rounded-full flex items-center justify-center leading-none text-xs font-medium hover:bg-white">
        <p>Try for free</p>
      </div>
      )}

      {!isMobile && (
      <div className="py-2 w-full sm:w-28 ml-14 px-4 mt-8 bg-neutral-100 dark:bg-neutral-800 border-2 border-solid border-black rounded-full flex items-center justify-center leading-none text-xs font-medium hover:bg-white">
        <p>Try for free</p>
      </div>
      )}
</Link>
    </div>
  );
};

export default Cart20;