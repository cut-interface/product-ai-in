import React, { FC } from "react";
import imgAdsDef from "@/images/banner.jpg";
import Image, { StaticImageData } from "next/image";

export interface SectionAdsProps {
  className?: string;
  imgAds?: string | StaticImageData;
}

const SectionAds: FC<SectionAdsProps> = ({
  className = "",
  imgAds = imgAdsDef,
}) => {
  return (
    <a
      href="/#"
      className={`nc-SectionAds block text-center mx-auto w-96 ${className}`}
    >
      <Image
        className="h-60  rounded-sm"
        src={imgAds}
        alt="Quảng cáo"
      />
    </a>
  );
};

export default SectionAds;
