"use client";

import React, { FC } from "react";
import Logo from "@/components/Logo/Logo";

export interface MainNav2LoggedProps {}

const LeftNav = () => {
    return (
      
      <div className="right-0 bg-white text-black p-3 h-screen w-16 border-2 border-solid border-gray-300">
        <ul className="list-none">
          <li className="mb-5 cursor-pointer hover:text-yellow-500">
    <span className="mb-2">
          <svg
            className="h-5 w-5 ml-1"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M22 22L20 20"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
  </span>
        <span className="hover:text-yellow-500 text-xs">Home</span>
      </li>
          

      <li className="mb-5 cursor-pointer hover:text-yellow-500">
    <span className="mb-2">
          <svg
            className="h-5 w-5 ml-1"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M22 22L20 20"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
  </span>
        <span className="hover:text-yellow-500 text-xs">Home</span>
      </li>

      <li className="mb-5 cursor-pointer hover:text-yellow-500">
    <span className="mb-2">
          <svg
            className="h-5 w-5 ml-1"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M22 22L20 20"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
  </span>
        <span className="hover:text-yellow-500 text-xs">Home</span>
      </li>


      <li className="mb-5 cursor-pointer hover:text-yellow-500">
    <span className="mb-2">
          <svg
            className="h-5 w-5 ml-1"
            viewBox="0 0 24 24"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M11.5 21C16.7467 21 21 16.7467 21 11.5C21 6.25329 16.7467 2 11.5 2C6.25329 2 2 6.25329 2 11.5C2 16.7467 6.25329 21 11.5 21Z"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <path
              d="M22 22L20 20"
              stroke="currentColor"
              strokeWidth="1.5"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
          </svg>
  </span>
        <span className="hover:text-yellow-500 text-xs">Home</span>
      </li>
          {/* Thêm các mục điều hướng khác tùy thuộc vào nhu cầu của bạn */}
        </ul>
      </div>
    );
  }
  
  export default LeftNav;


