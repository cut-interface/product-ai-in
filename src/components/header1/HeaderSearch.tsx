import React, { FC } from "react";
import Link from "next/link";
import Logo from "@/components/Logo/Logo";
import Navigation from "@/components/Navigation/Navigation";
import MenuBar from "@/components/MenuBar/MenuBar";
import AvatarDropdown from "./AvatarDropdown";

import NotifyDropdown from "./NotifyDropdown";
import Cloud from "./Cloud";
import About from "./About";


export interface MainNav1Props {}

const MainNav1: FC<MainNav1Props> = ({}) => {
  return (
<div className="nc-MainNav1 relative z-10 bg-white dark:bg-slate-900">
  <div className="container">
    <div className="h-20 py-5 flex justify-between items-center">
      <div className="flex items-center lg:hidden flex-1">
        <MenuBar />
      </div>

      <div className="flex justify-center lg:justify-start flex-1 items-center space-x-4 sm:space-x-10 2xl:space-x-14 rtl:space-x-reverse">
        <Logo />
        <Navigation className="hidden lg:flex" />
      </div>

      <div className="flex-1 flex items-center justify-end text-neutral-700 dark:text-neutral-100 space-x-1 rtl:space-x-reverse">
        <Link
          className="inline-flex items-center text-sm lg:text-[13px] font-medium text-gray-500 dark:text-slate-300 py-2.5 px-2 rounded-full hover:text-black dark:hover:bg-slate-800 dark:hover:text-slate-200"
          href={"/"}
        >
          Sell
        </Link>
        <Link
          className="inline-flex items-center text-sm lg:text-[13px] font-medium text-gray-500 dark:text-slate-300 py-2.5 px-2 rounded-full hover:text-black dark:hover:bg-slate-800 dark:hover:text-slate-200"
          href={"/"}
        >
          Pricing
        </Link>
        <About className="hidden md:block" />
        <AvatarDropdown />
      </div>
    </div>
  </div>
</div>
  );
};

export default MainNav1;
