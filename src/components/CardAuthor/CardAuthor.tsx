import React, { FC } from "react";
import { PostAuthorType } from "@/data/types";
import Avatar from "@/components/Avatar/Avatar";
import Link from "next/link";

export interface CardAuthorProps {
  className?: string;
  author: PostAuthorType;
}

const CardAuthor: FC<CardAuthorProps> = ({ className = "", author }) => {
  const { displayName, href = "/", avatar, jobName } = author;
  return (
    <Link
      href={href}
      className={`nc-CardAuthor flex items-center h-14 ${className}`}
    >
      <Avatar
        sizeClass="h-5 w-5 text-base"
        containerClassName="flex-shrink-0 me-4"
        radius="rounded-full"
        imgUrl={avatar}
        userName={displayName}
      />
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400`}
        >
          {jobName}
        </span>
      </div>
    </Link>
  );
};

export default CardAuthor;
