"use client";
import React, { FC, useState, useEffect } from "react";
import Card3 from "@/components/Card3/Card3";
import Heading1 from "@/components/Heading/HeadingList1";
import { DEMO_POSTS } from "@/data/posts";
import { PostDataType } from "@/data/types";
import SectionAds from "@/components/Sections/SectionAds";
import Cart from "./Cart";
import Menu from "./Menu";
import imgAdsDef from "@/images/banner.jpg";
import Image from "next/image";

const postsDemo: PostDataType[] = DEMO_POSTS.filter((_, i) => i > 7 && i < 15);

interface SectionLatestPostsProps {
  posts?: PostDataType[];
  heading?: string;
}

const SectionLatestPosts: FC<SectionLatestPostsProps> = ({
  posts = postsDemo,
  heading = "All Apps >",
}) => {
  const [isWidgetVisible, setWidgetVisibility] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  const [isTablet, setIsTablet] = useState(false);

  const toggleVisibility = () => {
    console.log("Toggling visibility");
    setWidgetVisibility(!isWidgetVisible);
  };

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768);
      setIsTablet(window.innerWidth >= 768 && window.innerWidth <= 1024);

      setWidgetVisibility(window.innerWidth >= 768);
    };

    handleResize();

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div className="relative flex">
      <div className="flex flex-col lg:flex-row">
        {(isMobile || isTablet) && isWidgetVisible && (
          <div className="fixed z-10 w-56 bg-white border-black border-solid rounded-lg top-32 left-4 h-4/6 border-1">
            <Menu className="left-0 w-56 border-solid border-x-2" />
          </div>
        )}

        {!isMobile && !isTablet && (
          <div className="fixed block w-56 h-screen border-black border-solid rounded-lg top-20 left-16 border-1">
            <Menu className="left-0 w-56 border-solid border-x-2" />
          </div>
        )}

        <div
          className={`relative w-full ${isMobile ? "lg:w-full" : "lg:w-full"}`}
        >
          <div className="">
            {isMobile && (
              <div className="fixed left-0 z-30 w-screen h-12 border-solid top-20 border-y-2 bg-gray-50">
                <Heading1 onClick={toggleVisibility}>{heading}</Heading1>
              </div>
            )}

            {isTablet && (
              <div className="fixed left-0 z-30 w-screen h-16 border-solid border-y-2 bg-gray-50">
                <Heading1 onClick={toggleVisibility}>{heading}</Heading1>
              </div>
            )}

            {!isMobile && !isTablet && (
              <div className="fixed z-30 w-screen border-solid left-72 border-y-2 bg-gray-50">
                <Heading1>{heading}</Heading1>
              </div>
            )}
          </div>

          {isMobile && (
            <a href="/#" className="block nc-SectionAds ">
              <Image
                className="w-full mt-16 rounded-lg h-52"
                src={imgAdsDef}
                alt="ads"
                layout=""
              />
            </a>
          )}

          {!isMobile && !isTablet && (
            <a
              href="/#"
              className="block ml-[270px] mt-20 nc-SectionAds xl:ml-[280px] xl:w-full xl:pr-[215px]"
            >
              <Image
                className="w-full rounded-lg h-52"
                src={imgAdsDef}
                alt="ads"
                layout=""
              />
            </a>
          )}

          {isTablet && (
            <a href="/#" className="block mt-20  ml-[40px] nc-SectionAds">
              <Image
                className="w-full rounded-lg h-52"
                src={imgAdsDef}
                alt="ads"
                layout=""
              />
            </a>
          )}

          {isMobile && (
            <div className={`relative flex-wrap flex grid-cols-1 mt-8`}>
              {posts
                .filter((_, i) => i > 0)
                .map((p, index) => (
                  <Cart className="w-full md:w-[400px]" key={index} post={p} />
                ))}
            </div>
          )}

          {!isMobile && !isTablet && (
            <div
              className={`relative flex flex-wrap gap-x-4 gap-y-[20px] sm:gap-x-2 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 mt-8 mb-28 ml-[40px] ${
                isMobile ? "0" : "10"
              }`}
            >
              {posts
                .filter((_, i) => i > 0)
                .map((p, index) => (
                  <Cart
                    className="w-full lg:w-[350px] md:w-[300px] sm:w-[300px] lg:left-60"
                    key={index}
                    post={p}
                  />
                ))}
            </div>
          )}

          {isTablet && (
            <div
              className={`relative flex flex-wrap gap-x-4 gap-y-[20px] sm:gap-x-2 grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 mt-8 mb-28 ml-[60px] mr-20px${
                isMobile ? "0" : "10"
              }`}
            >
              {posts
                .filter((_, i) => i > 0)
                .map((p, index) => (
                  <Cart className="w-full" key={index} post={p} />
                ))}
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default SectionLatestPosts;
