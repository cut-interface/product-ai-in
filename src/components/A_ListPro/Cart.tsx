import React, { FC, useState } from "react";
import { PostDataType } from "@/data/types";
import Link from "next/link";
import ButtonPlayMusicPlayer from "../ButtonPlayMusicPlayer";
import Image from "next/image";
import { PauseIcon, PlayIcon } from "@heroicons/react/24/solid";

export interface Card15PodcastProps {
  className?: string;
  post: PostDataType;
}

const Card15Podcast: FC<Card15PodcastProps> = ({
  className = "h-40 w-80",
  post,
}) => {
  const { title, href, featuredImage, postType, date } = post;
  const IS_AUDIO = postType === "audio";

  const [isHovered, setIsHovered] = useState(false);

  const renderDefaultBtnListen = (state?: "playing") => {
    return (
      <div className="inline-flex items-center mt-3 pe-4 py-0.5 hover:ps-0.5 cursor-pointer rounded-sm transition-all hover:bg-primary-50 dark:hover:bg-neutral-900">
        <span className="flex items-center justify-center w-8 h-8 rounded-sm bg-primary-50 dark:bg-neutral-800 text-primary-6000 dark:text-primary-200">
          {state === "playing" ? (
            <PauseIcon className="w-5 h-5" />
          ) : (
            <PlayIcon className="w-5 h-5 rtl:rotate-180" />
          )}
        </span>

        <span className="text-xs font-medium ms-3 sm:text-sm">
          {state === "playing" ? "Now playing" : "Listen now"}
        </span>
      </div>
    );
  };

  return (
    <div
      className={`nc-Card15Podcast relative group items-center mt-4 p-3 rounded-sm border-2  hover:border-gray-300 border-neutral-200 dark:border-neutral-700 bg-white dark:bg-neutral-900 ${className}`}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div className="flex flex-col sm:flex-row">
        <Link
          href={"/product"}
          className="relative block overflow-hidden rounded-sm shadow-lg w-9 sm:w-9 h-9"
        >
          <Image
            className="object-cover w-full h-full"
            src={featuredImage}
            fill
            alt={title}
            sizes="100px"
          />
        </Link>

        <div className="mt-3 sm:ml-3 sm:mt-0">
          <h2 className="block mt-2 text-sm font-semibold sm:text-sm sm:mt-4">
            Product
          </h2>
        </div>
      </div>

      <div className="flex flex-col flex-grow mt-2">
        <p
          className={`nc-card-title block font-semibold text-xs sm:text-xs h-8 overflow-hidden ${
            IS_AUDIO ? "line-clamp-1" : "line-clamp-2"
          }`}
        >
          <Link href={"/product"} title={title}>
            {title}
          </Link>
        </p>
      </div>
      <div className="w-full mt-3 border-b border-neutral-200 dark:border-neutral-700" />

      <div className="flex mt-2">
        <div
          className={`ml-auto w-full sm:w-28 py-2 px-4 mt-3 dark:bg-neutral-800 rounded-full flex items-center justify-center leading-none text-xs font-medium hover:bg-gray-300 ${
            isHovered ? "visible" : "invisible"
          }`}
        >
          <Link href={"/blog"}>
            <p>Learn More</p>
          </Link>
        </div>
        <div className="flex items-center justify-center w-full px-4 py-2 mt-3 ml-0 text-xs font-medium leading-none border-2 border-solid rounded-full sm:w-28 sm:ml-2 bg-neutral-100 dark:bg-neutral-800 hover:bg-white">
          <Link href={"/product"}>
            <p>Learn</p>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Card15Podcast;
