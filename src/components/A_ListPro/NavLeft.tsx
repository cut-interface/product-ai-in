"use client"
import React, { FC, useState, useEffect } from "react";
import CardAuthor from "@/components/CardAuthor/CardAuthor";
import WidgetHeading1 from "@/components/WidgetHeading1/WidgetHeading1";
import { DEMO_AUTHORS } from "@/data/authors";
import { PostAuthorType } from "@/data/types";
import Link from "next/link";

const authorsDemo: PostAuthorType[] = DEMO_AUTHORS.filter((_, i) => i < 2);

export interface WidgetAuthorsProps {
  className?: string;
  authors?: PostAuthorType[];
}


const WidgetAuthors: FC<WidgetAuthorsProps> = ({
  className = "bg-neutral-100 dark:bg-neutral-800",
  authors = authorsDemo,
}) => {
  const [isMobile, setIsMobile] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768); // Adjust the threshold as needed
    };

    handleResize(); // Initial check
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <div
      className={`nc-WidgetAuthors h-full rounded-sm overflow-hidden ${
        isMobile ? "container" : ""
      } ${className}`}
      style={{ maxHeight: "720px", overflowY: "auto" }}
    >
      <WidgetHeading1
        title="Apps"
      />
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>



<WidgetHeading1
        title="FEATURED"
      />
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
  {authors.map((author) => (
    <CardAuthor
      className="p-2 xl:p-3 hover:bg-neutral-200 dark:hover:bg-neutral-700"
      key={author.id}
      author={author}
    />
  ))}
</div>

<WidgetHeading1
        title="Category"
      />
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>
<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>
</div>

<WidgetHeading1
        title="Category"
      />
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700">
  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>  <div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>
<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg
      viewBox="0 0 512 512"
      fill="currentColor"
      height="1em"
      width="1em"
    >
      <path d="M104 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 160a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 312a56 56 0 1156-56 56.06 56.06 0 01-56 56zM104 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM256 464a56 56 0 1156-56 56.06 56.06 0 01-56 56zM408 464a56 56 0 1156-56 56.06 56.06 0 01-56 56z" />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Apps
        </span>
      </div>
    </Link>
</div>

<div className="flex flex-col divide-y divide-neutral-200 dark:divide-neutral-700 xl:p-3 hover:bg-neutral-200">
  <Link
      href="/"
      className={`nc-CardAuthor flex items-center ${
        isMobile ? "mt-4" : ""
      } ${className}`}
    >
      <span>
      <svg fill="none" viewBox="0 0 15 15" height="1em" width="1em">
      <path
        fill="currentColor"
        fillRule="evenodd"
        d="M1.903 7.297c0 3.044 2.207 5.118 4.686 5.547a.521.521 0 11-.178 1.027C3.5 13.367.861 10.913.861 7.297c0-1.537.699-2.745 1.515-3.663.585-.658 1.254-1.193 1.792-1.602H2.532a.5.5 0 010-1h3a.5.5 0 01.5.5v3a.5.5 0 01-1 0V2.686l-.001.002c-.572.43-1.27.957-1.875 1.638-.715.804-1.253 1.776-1.253 2.97zm11.108.406c0-3.012-2.16-5.073-4.607-5.533a.521.521 0 11.192-1.024c2.874.54 5.457 2.98 5.457 6.557 0 1.537-.699 2.744-1.515 3.663-.585.658-1.254 1.193-1.792 1.602h1.636a.5.5 0 110 1h-3a.5.5 0 01-.5-.5v-3a.5.5 0 111 0v1.845h.002c.571-.432 1.27-.958 1.874-1.64.715-.803 1.253-1.775 1.253-2.97z"
        clipRule="evenodd"
      />
    </svg>
      </span>
      <div>
        <span
          className={`block mt-[2px] text-xs text-neutral-500 dark:text-neutral-400 ml-2`}
        >
          Update
        </span>
      </div>
    </Link>
</div>
</div>




    </div>

    
  );
};

export default WidgetAuthors;
